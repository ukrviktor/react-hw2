export class Service {
  static async getAll() {
    const response = await fetch('/db/db.json');
    return response;
  }
}
