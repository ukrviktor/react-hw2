import React, { Component } from 'react';
import styles from './Header.module.scss';
import PropTypes from 'prop-types';

export class Header extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={styles.HeaderContainer}>
        <div className={styles.Header}>
          <div className={styles.HeaderBasket}>
            <i className="fa-solid fa-basket-shopping"></i>
            <span className={styles.basketValue}>
              {this.props.goodsBasketCount}
            </span>
          </div>
          <div className={styles.HeaderStar}>
            <i className="fa-solid fa-star"></i>
            <span className={styles.starValue}>
              {this.props.goodsStarCount}
            </span>
          </div>
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  goodsBasketCount: PropTypes.number.isRequired,
  goodsStarCount: PropTypes.number.isRequired,
};
